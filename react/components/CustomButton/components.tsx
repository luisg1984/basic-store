import styled from "styled-components";

export const Button = styled.button`
  color: #ffffff;
  background-color: #3483fa;
  font-size: 1rem;
  margin: 1em;
  padding: 0.25em 1em;
  border: none;
  padding: 10px 80px 10px 80px;
  border-radius: 6px ;
  cursor: pointer;
`;
export const ContainerFlexCenter = styled.div`
  display: flex;
  width: 100%;
  justify-content: center;
`;
