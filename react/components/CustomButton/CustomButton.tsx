import React from "react";
import { IButtonProps } from "./interface";
import { Button, ContainerFlexCenter } from "./components";

const CustomButton: React.FC = ({ buttonText, url }: IButtonProps) => {
  return (
    <ContainerFlexCenter>
      <a href={url} target="_blank">
        <Button>{buttonText}</Button>
      </a>
    </ContainerFlexCenter>
  );
};

export default CustomButton;
