import gql from "graphql";
export const QUERY_VALUE = gql`
  query hello {
    hero {
      name
      height
    }
  }
`;
